FROM ubuntu:20.04

RUN apt update -y 
RUN apt install -y python3-pip

COPY ./requirments.txt /app/requirments.txt

WORKDIR /app

RUN pip install -r requirments.txt

COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]